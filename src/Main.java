
public class Main {
	public static void main(String[] args) {
		Amazon ku = new KUgreenTea();
		Amazon cu = new CUcoffee();
		AmazonProduct greenteaKU = ku.create();
		AmazonProduct coffeeCU = cu.create();
		System.out.println(greenteaKU.toString());
		System.out.println(coffeeCU.toString());
	}
}
